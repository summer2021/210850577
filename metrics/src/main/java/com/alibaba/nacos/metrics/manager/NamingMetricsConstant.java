/*
 * Copyright 1999-2018 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alibaba.nacos.metrics.manager;

/**
 * Naming Metrics Constant.
 *
 * @author holdonbei
 */
public class NamingMetricsConstant {
    
    /**
     * metrics name.
     */
    public static final String NACOS_MONITOR = "nacos_monitor";
    
    /**
     * metrics name.
     */
    public static final String NACOS_EXCEPTION = "nacos_exception";
    
    /**
     * metrics name.
     */
    public static final String NACOS_SERVER_PUSH = "nacos_server_push";
    
    /**
     * metrics name.
     */
    public static final String NACOS_SERVER_PUSH_COUNT = "nacos_server_push_count";
    
    /**
     * metrics tag key.
     */
    public static final String MODULE = "module";
    
    /**
     * metrics tag key.
     */
    public static final String NAME = "name";
    
    /**
     * metrics tag key.
     */
    public static final String TYPE = "type";
    
    /**
     * metrics tag key.
     */
    public static final String SUCCESS = "success";
    
    /**
     * metrics tag value.
     */
    public static final String NAMING = "naming";
    
    /**
     * metrics tag value.
     */
    public static final String MYSQL_HEALTH_CHECK = "mysqlHealthCheck";
    
    /**
     * metrics tag value.
     */
    public static final String HTTP_HEALTH_CHECK = "httpHealthCheck";
    
    /**
     * metrics tag value.
     */
    public static final String TCP_HEALTH_CHECK = "tcpHealthCheck";
    
    /**
     * metrics tag value.
     */
    public static final String SERVICE_COUNT = "serviceCount";
    
    /**
     * metrics tag value.
     */
    public static final String IP_COUNT = "ipCount";
    
    /**
     * metrics tag value.
     */
    public static final String SUBSCRIBER_COUNT = "subscriberCount";
    
    /**
     * metrics tag value.
     */
    public static final String MAX_PUSH_COST = "maxPushCost";
    
    /**
     * metrics tag value.
     */
    public static final String AVG_PUSH_COST = "avgPushCost";
    
    /**
     * metrics tag value.
     */
    public static final String LEADER_STATUS = "leaderStatus";
    
    /**
     * metrics tag value.
     */
    public static final String TOTAL_PUSH = "totalPush";
    
    /**
     * metrics tag value.
     */
    public static final String FAILED_PUSH = "failedPush";
    
    /**
     * metrics tag value.
     */
    public static final String TOTAL_PUSH_COUNT_FOR_AVG = "totalPushCountForAvg";
    
    /**
     * metrics tag value.
     */
    public static final String TOTAL_PUSH_COST_FOR_AVG = "totalPushCostForAvg";
    
    /**
     * metrics tag value.
     */
    public static final String DISK = "disk";
    
    /**
     * metrics tag value.
     */
    public static final String LEADER_SEND_BEAT_FAILED = "leaderSendBeatFailed";
    
    /**
     * metrics tag value.
     */
    public static final String GRPC = "grpc";
    
    /**
     * metrics tag value.
     */
    public static final String UDP = "udp";
    
    /**
     * metrics tag value.
     */
    public static final String TRUE = "true";
    
    /**
     * metrics tag value.
     */
    public static final String FALSE = "false";
    
}
