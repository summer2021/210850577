/*
 * Copyright 1999-2018 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alibaba.nacos.metrics.manager;

/**
 * Config Metrics Constant.
 *
 * @author holdonbei
 */
public class ConfigMetricsConstant {
    
    /**
     * metrics name.
     */
    public static final String NACOS_MONITOR = "nacos_monitor";
    
    /**
     * metrics name.
     */
    public static final String NACOS_TIMER = "nacos_timer";
    
    /**
     * metrics name.
     */
    public static final String NACOS_EXCEPTION = "nacos_exception";
    
    /**
     * metrics tag key.
     */
    public static final String MODULE = "module";
    
    /**
     * metrics tag key.
     */
    public static final String NAME = "name";
    
    /**
     * metrics tag value.
     */
    public static final String CONFIG = "config";
    
    /**
     * metrics tag value.
     */
    public static final String GET_CONFIG = "getConfig";
    
    /**
     * metrics tag value.
     */
    public static final String PUBLISH = "publish";
    
    /**
     * metrics tag value.
     */
    public static final String LONG_POLLING = "longPolling";
    
    /**
     * metrics tag value.
     */
    public static final String CONFIG_COUNT = "configCount";
    
    /**
     * metrics tag value.
     */
    public static final String NOTIFY_TASK = "notifyTask";
    
    /**
     * metrics tag value.
     */
    public static final String NOTIFY_CLIENT_TASK = "notifyClientTask";
    
    /**
     * metrics tag value.
     */
    public static final String DUMP_TASK = "dumpTask";
    
    /**
     * metrics tag value.
     */
    public static final String NOTIFY_RT = "notifyRt";
    
    /**
     * metrics tag value.
     */
    public static final String ILLEGAL_ARGUMENT = "illegalArgument";
    
    /**
     * metrics tag value.
     */
    public static final String NACOS = "nacos";
    
    /**
     * metrics tag value.
     */
    public static final String DB = "db";
    
    /**
     * metrics tag value.
     */
    public static final String CONFIG_NOTIFY = "configNotify";
    
    /**
     * metrics tag value.
     */
    public static final String UNHEALTH = "unhealth";
    
}